clear all
close all
nelx=5;
nely=5;
nelz=5;
volfrac=0.25;
penal=2;
NN=(nelz+1)*(nely+1)*(nelx+1);
E0 = 1; Emin = 1e-6; nu = 0.3;
[xc,yc,zc]=meshgrid(-2:0.5:nelx+2, -2:0.5:nely+2,-2:0.5:nely+2);
carriers = [xc(:)';yc(:)';zc(:)'; 0.16 * volfrac * ones(1, numel(xc))];
index=carriers>-0.1&carriers<10.1;
I=sum(index(1:end,:));
I=find(I==4);
F = sparse(3*(nely+1)*(nelx + 1)*(nelz)+3:3:3*(nely+1)*(nelx+1)*(nelz+1), 1, -0.01, 3*(nely+1)*(nelx+1)*(nelz+1),1);% Force
U = zeros(3*(nely+1)*(nelx+1)*(nelz+1), 1);
freedofs = setdiff(1:3*(nely+1)*(nelx+1)*(nelz+1),1:3*(nely+1)*(nelx+1));% set boundary condition
dPdF=E0 / (1+nu) / (1-2*nu) *[1-nu, 0, 0, 0, nu, 0, 0, 0, nu;
                              0, (1-2*nu)/2, 0, (1-2*nu)/2, 0, 0, 0, 0, 0;
                              0, 0, (1-2*nu)/2, 0, 0, 0, (1-2*nu)/2, 0, 0;
                              0, (1-2*nu)/2, 0, (1-2*nu)/2, 0, 0, 0, 0, 0;
                              nu, 0, 0, 0, 1-nu, 0, 0, 0, nu;
                              0, 0, 0, 0, 0, (1-2*nu)/2, 0, (1-2*nu)/2, 0;
                              0, 0, (1-2*nu)/2, 0, 0, 0, (1-2*nu)/2, 0, 0;
                              0, 0, 0, 0, 0, (1-2*nu)/2, 0, (1-2*nu)/2, 0;
                              nu, 0, 0, 0, nu, 0, 0, 0, 1-nu];
 KE={zeros(24), zeros(24), zeros(24), zeros(24), zeros(24), zeros(24), zeros(24), zeros(24)};
 localXq = [0 0 0 0 1 1 1 1; 0 0 1 1 0 0 1 1 ; 0 1 0 1 0 1 0 1]*0.5 + 0.25;
 
 for iq = 1:8
     xq = localXq(:, iq);
     dw = [-xq(2)*xq(3), xq(2)*xq(3), (1-xq(2))*xq(3), (xq(2)-1)*xq(3), (xq(3)-1)*xq(2), (1-xq(3))*xq(2), (1-xq(2))*(1-xq(3)), (xq(2)-1)*(1-xq(3));
           (1-xq(1))*xq(3), xq(1)*xq(3), -xq(1)*xq(3), (xq(1)-1)*xq(3), (1-xq(1))*(1-xq(3)), xq(1)*(1-xq(3)), (xq(3)-1)*xq(1), (xq(1)-1)*(1-xq(3));
           (1-xq(1))*xq(2), xq(1)*xq(2), (1-xq(2))*xq(1), (1-xq(2))*(1-xq(1)), (xq(1)-1)*xq(2), -xq(1)*xq(2), (xq(2)-1)*xq(1), (xq(1)-1)*(1-xq(2))];
     for in = 1:8
     for jn = 1:8
         dFdX = zeros(3);
         for id = 1:3
         for jd = 1:3
             dFdX = dFdX + dPdF(3*id-2:3*id,3*jd-2:3*jd)*dw(id, in)* dw(jd, jn);
         end
         end
         KE{iq}(3*in-2:3*in, 3*jn-2:3*jn) = KE{iq}(3*in-2:3*in, 3*jn-2:3*jn) + 0.125*dFdX;
     end
     end
 end
 nodenrs = zeros(nelx+1,nely+1,nelz+1);
 x=1;
%  nodenrs = reshape(1:(1+nelx)*(1+nely)*(1+nelz),1+nelz,1+nely,1+nelx);
 for i=1:nelx+1
     for j=1:nely+1
         for k=1:nelz+1
             nodenrs(i,j,k)=x;
             x=x+1;
         end
     end
 end
 
             
 edofVec = reshape(3*nodenrs(1:end-1,1:end-1,1:end-1)+1,nelx*nely*nelz,1);
 edofMat = repmat(edofVec,1,24)+repmat([0 1 2 3*nely+[3 4 5 0 1 2] -3 -2 -1 3*(nelx+1)*(nely+1)+[0 1 2 3*nely+[3 4 5 0 1 2] -3 -2 -1]],nelx*nely*nelz,1);
 iK = reshape(kron(edofMat,ones(24,1))',576*nelx*nely*nelz,1);
 jK = reshape(kron(edofMat,ones(1,24))',576*nelx*nely*nelz,1);
 xold1 = carriers; xold2 = xold1; low = xold1; upp = xold1;
 iter = 0; changes = 1e10*ones(10,1); relCh=1;
while (relCh > 1e-3 && iter < 200)
 iter = iter + 1;
 dmPow = min(ceil(iter/20), 10); % power of the density map
 baseNode = round(carriers(1:3, :)) + 1;
 [pollutedCellXLocal, pollutedCellYLocal, pollutedCellZLocal] = meshgrid(-3:2, -3:2, -3:2);
 pollutedCellLocal = [pollutedCellXLocal(:)'; pollutedCellYLocal(:)'; pollutedCellZLocal(:)'];
 pollutedCell = repmat(baseNode, 216, 1) + pollutedCellLocal(:);
 pollutedCell = reshape(pollutedCell, 3, []);
 pollutedCell(1, :) = mod(pollutedCell(1, :)-1, nelx)+1;
 pollutedCell(2, :) = mod(pollutedCell(2, :)-1, nely)+1;
 pollutedCell(3, :) = mod(pollutedCell(3, :)-1, nelz)+1;
 pollutedCellIndex = (pollutedCell(1, :) - 1) * nely + pollutedCell(2, :)+(pollutedCell(3,:)-1)*nelx*nely;
 pollutedQuadIndex = 8*(pollutedCellIndex)+[-7;-6;-5;-4;-3;-2;-1;0];
 quadPos = reshape((repmat(pollutedCell,8,1)-1+localXq(:)),648*8,[]);
 carrierShift = reshape(repmat(carriers(1:3,:),216*8,1)-quadPos,3,[]);
 R = vecnorm(carrierShift)/1.25;
 W = 1/(pi)*((3/4*R.^3-3/2*R.^2+1).*(R < 1) + 1/4*(2-R).^3.*((R>=1)&(R<2)));
 dW_div_R =  1/(pi)*((3*R/2-2).*(R < 1) - 1/2*((2-R).^2./R).*((R>=1)&(R<2)));
 x = accumarray(pollutedQuadIndex(:), reshape(reshape(W, 216*8,[]).*carriers(4,:),[],1));
 if dmPow == 1
        quadRho = x .* (x<=0.9) + (-2.5*(x-0.9).^2+x) .* ((0.9<x)&(x<1.1)) + (x>=1.1);
        dDM =     1 .* (x<=0.9) + ( -5 *(x-0.9)   +1) .* ((0.9<x)&(x<1.1)); 
 else
        quadRho = 0.5*(2*x).^dmPow    .* (x<=0.5) + (1-0.5*(2-2*x).^dmPow)    .* ((x>0.5)&(x < 1)) + (x>=1);
        dDM =   dmPow*(2*x).^(dmPow-1).* (x<=0.5) + (dmPow*(2-2*x).^(dmPow-1)).*((x>0.5)&(x < 1));
 end
 quadRho = reshape(quadRho, 8, nelx * nely * nelz); dDM = reshape(dDM, 8, nelx * nely * nelz);
 T=(reshape(repmat(carriers(4, :),216*8,1), size(W)).*dW_div_R/1.25^2).*carrierShift;
 dQuadRhoNaiveCom = [(reshape(repmat(carriers(3, :),216*8,1), size(W)).*dW_div_R/1.25^2).*carrierShift; W];
 sK = zeros(numel(iK),1);
 for iq = 1:8
        sK = sK + reshape(KE{iq}(:)*(Emin+ quadRho(iq,:).^penal * (E0-Emin)),576*nelx*nely*nelz,1);
 end
 K = sparse(iK,jK,sK,3*NN,3*NN);
 spy(K);
 K = (K+K')/2;
 spy(K);
 U(freedofs) = K(freedofs,freedofs)\F(freedofs);
 ce = zeros(size(quadRho));

 for iq = 1:8
     ce(iq,:) = sum((U(edofMat)*KE{iq}).*U(edofMat),2);
 end
 
 c = sum(ce.*(Emin+quadRho.^penal*(E0-Emin)),[1,2]);
 v = sum(quadRho,[1,2])/numel(quadRho);
 dCdRhoNaive = -penal*(E0-Emin) * reshape(quadRho.^(penal-1).*dDM.*ce,[], 1);
 dC = reshape(sum(reshape(dQuadRhoNaiveCom .* dCdRhoNaive(pollutedQuadIndex(:))', 4, 216*8, []), 2), [], 1);
 dVdRhoNaive = reshape(dDM ./ numel(quadRho), [], 1);
 dV = reshape(sum(reshape(dQuadRhoNaiveCom .* dVdRhoNaive(pollutedQuadIndex(:))', 4, 216*8, []), 2), [], 1);
 xmin = [carriers(1:3, :) - min(nelx, nely) / 40; max(carriers(4,:) - 0.5, 0)];
 xmax = [carriers(1:3, :) + min(nelx, nely) / 40; carriers(4,:) + 0.5];
 dC_norm = max(abs(dC(:))); dV_norm = max(abs(dV(:)));
 [carriers(:), low(:), upp(:), xold1(:), xold2(:)] = mmaUpdate(carriers(:), dC(:)/dC_norm, ... 
        (v - volfrac)/dV_norm, dV(:)/dV_norm, xmin(:), xmax(:), iter, low(:), upp(:), xold1(:), xold2(:));
 changes(mod(iter-1, 10) + 1) = c; relCh = min(1,(max(changes) - min(changes)) / min(changes));
 fprintf(' It.:%5i Obj.:%11.4f Vol.:%7.3f dC_norm: %7.3f relCh: %7.3f\n',iter,c,v, dC_norm, relCh);   
 
end

scatter3(carriers(1,:),carriers(2,:),carriers(3,:),10,carriers(4,:));

xlim([0 nelx]) 
ylim([0 nely])
zlim([0 nelz])

function [xval, low, upp, xold1, xold2] = mmaUpdate(xval, dfdx, gx, dgdx, xmin, xmax, iter, low, upp, xold1, xold2)
%% parameters
n = length(xval);
m = length(gx);
asyminit = 0.02;
asymdec = 0.65;
asyminc = 1.05;
xmamieps = 1.0e-5;
epsimin = sqrt(n + m) * 1e-9;
raa0 = 0.0001;
albefa = 0.4;
a = zeros(1,m);
c = 1000 * ones(1,m);

%% Generate the subproblem
if iter < 3
    low = xval - asyminit * (xmax - xmin);
    upp = xval + asyminit * (xmax - xmin);
else
    zzz = (xval - xold1) .* (xold1 - xold2);
    gamma = ones(n, 1);
    gamma(zzz < 0) = asymdec;
    gamma(zzz > 0) = asyminc;
    low = xval - gamma .* (xold1 - low);
    upp = xval + gamma .* (upp - xold1);
    xmami = max(xmamieps, xmax - xmin);
    low = max(low, xval - 10.0 * xmami);
    low = min(low, xval - 0.01 * xmami);
    upp = max(upp, xval + 0.01 * xmami);
    upp = min(upp, xval + 10.0 * xmami);
end

% Set bounds and the coefficients for the approximation
alpha = max(xmin, low + albefa * (xval - low));
beta = min(xmax, upp - albefa * (upp - xval));

% objective function
dfdxp = max(0.0, dfdx); %p0
dfdxm = max(0.0, -1.0 * dfdx); %q0
xmamiinv = 1.0 ./ max(xmamieps, xmax - xmin);
pq = 0.001 * (dfdxp + dfdxm) + raa0 * xmamiinv;
p0 = (upp - xval) .^ 2.0 .* (dfdxp + pq);
q0 = (xval - low) .^ 2.0 .* (dfdxm + pq);

% Constraints
dgdxp = max(0.0, dgdx);
dgdxm = max(0.0, -1.0 * dgdx);
xmamiinv = 1.0 ./ max(xmamieps, xmax - xmin);
pq = 0.001 * (dgdxp + dgdxm) + raa0 * xmamiinv;
pij = (upp - xval).^2 .* (dgdxp + pq);
qij = (xval - low).^2 .* (dgdxm + pq);

b = -gx + sum(pij ./ (upp - xval) + qij ./ (xval - low), 1);
xold2 = xold1;
xold1 = xval;

%% Solve the dual with an interior point method
lam = c/2;
mu = ones(1,m);
tol = epsimin;
epsi = 1.0;
err = 1.0;
while epsi > tol
    loop = 0;
    while (err > 0.9 * epsi) && (loop < 300)
        loop = loop + 1;
        % Set up Newton system 
        % XYZofLAMBDA
        lam = max(0, lam);
        y = max(0, lam - c); % Note y=(lam-c)/d - however d is fixed at one !!
        lamai = dot(lam, a);
        z = max(0, 10 * (lamai - 1)); % SINCE a0 = 1.0
        pjlam = p0 + sum(pij .* lam, 2);
        qjlam = q0 + sum(qij .* lam, 2);
        xval = (sqrt(pjlam) .* low + sqrt(qjlam) .* upp) ./ (sqrt(pjlam) + sqrt(qjlam));
        xval = max(alpha, xval);
        xval = min(beta, xval);
        % DualGrad
        grad = -b - a * z - y + sum(pij ./ (upp - xval) + qij ./ (xval - low), 1);
        grad = - grad - epsi ./ lam;
        % DualHess
        PQ = pij ./ (upp - xval).^2 - qij ./ (xval - low).^2;
        df2 = -0.5 ./ (pjlam ./ (upp - xval).^3 + qjlam ./ (xval - low) .^3);
        xp = (sqrt(pjlam) .* low + sqrt(qjlam) .* upp) ./ (sqrt(pjlam) + sqrt(qjlam));
        df2(xp < alpha) = 0;
        df2(xp > beta) = 0;
        % Create the matrix/matrix/matrix product: PQ^T * diag(df2) * PQ
        hess = PQ' * (df2 .* PQ);
        lam = max(0, lam);
        lamai = lamai + dot(lam, a);
        diag_mod = zeros(1, m);
        diag_mod(lam > c) = -1;
        diag_mod = diag_mod - mu./lam;
        hess = hess + diag(diag_mod);
        if lamai > 0
            hess = hess - 10 *  a' * a;
        end
        HessCorr = 1e-4 * trace(hess) / m;
        if HessCorr > -1.0e-7
            HessCorr = -1.0e-7;
        end
        hess = hess + eye(m) * HessCorr;
        % Solve Newton system
        s = hess \ grad';
        % Get the full search direction
        s = [s'; -mu + epsi ./ lam - s' .* mu ./ lam];
        % DualLineSearch
        theta = 1 / max([1.005, -1.01 * s(1, :) ./ lam, -1.01 * s(2, :) ./ mu]);
        lam = lam + theta * s(1,:);
        mu = mu + theta * s(2, :);
        % XYZofLAMBDA
        lam = max(0, lam);
        y = max(0, lam - c); % Note y=(lam-c)/d - however d is fixed at one !!
        lamai = dot(lam, a);
        z = max(0, 10 * (lamai - 1)); % SINCE a0 = 1.0
        pjlam = p0 + sum(pij .* lam, 2);
        qjlam = q0 + sum(qij .* lam, 2);
        xval = (sqrt(pjlam) .* low + sqrt(qjlam) .* upp) ./ (sqrt(pjlam) + sqrt(qjlam));
        xval = max(alpha, xval);
        xval = min(beta, xval);
        % DualResidual
        res = [-b - a * z - y + mu + sum(pij ./ (upp - xval) + qij ./ (xval - low), 1); mu .* lam - epsi];
        err = max(abs(res(:)));
    end
    epsi = epsi * 0.1;
end
end
 
 
 
 
 
 
 
 
    
    
    
    
    